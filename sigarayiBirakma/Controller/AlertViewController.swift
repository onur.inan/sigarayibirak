//
//  AlertViewController.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 18.05.2021.
//

import UIKit

class AlertViewController: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtMoney: UITextField!
    
    var name = String()
    var money = String()
    
    
    var btnAction:(()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
  
    
    
    @IBAction func btnCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        var title=""
        var message=""
        
        if isEmpty()==true {
            title="Hata"
            message="Lütfen bilgileri eksikswiz giriniz!"
            showAlert(title: title, message: message)
            
        }else{
            
            let name=txtName.text ?? ""
            let money=txtMoney.text ?? ""
            
            let award=Award(id: "", name: name, money: money)
            let isSave=DBManager.getInstance().saveAward(award: award)
            print("Awart Save-> \(isSave)" )
            
            btnAction?()
            dismiss(animated: true, completion: nil)
        }
       
      
        
    }
    func isEmpty()->Bool{
        if (txtName.text == "" || txtMoney.text == ""
        )
         {
          return true
        }else{
            return false
        }
    }
    
    func showAlert(title:String,message:String){
        let alert=UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction=UIAlertAction(title: "Tamam", style: .default, handler: nil)
        alert.addAction(alertAction)
        present(alert, animated: true, completion: nil)
    }
    
}
