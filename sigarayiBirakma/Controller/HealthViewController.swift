//
//  HealthViewController.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 15.04.2021.
//

import UIKit

class HealthViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!

    var decisionyear=userDefault.DecisionYear
    var heards=[Heard]()
    var dateNow=Date()
    var dateBegin=Date()
    var targerDate=Date()
    var diffs = TimeInterval()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate=self
        tblView.dataSource=self
        heards=DBManager.getInstance().getAllHeardData()
        
        
        dateBegin=stringtoDate(dateString: decisionyear ?? "")
        dateNow=Date()
        diffs = dateNow.timeIntervalSince(dateBegin)
    }
    
    

    func stringtoDate(dateString:String)   -> Date{
        let df=DateFormatter()
        df.dateFormat="dd/MM/yyyy HH:mm"
        guard let date=df.date(from: dateString) else { return Date() }
        return date
                
    }
    
  
    

}
extension HealthViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        heards.count
    }

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let persent = (diffs * 100 / Double(heards[indexPath.row].timeSecond))
        targerDate=dateBegin.addingTimeInterval(TimeInterval(heards[indexPath.row].timeSecond))
       
        
        let cell=tableView.dequeueReusableCell(withIdentifier: "HealthCell", for: indexPath) as! HealthTableViewCell
        cell.lblInfo.text=heards[indexPath.row].info
        if (persent>100){
            cell.lblPercent.text="100%"
        }else{
            cell.lblPercent.text=String(format: "%.0f", persent) + "%"
        }
        cell.progressPercent.progress=Float(persent / 100)
        cell.lblDate.text = targerDate.toString(fromFormat: "dd/MM/yyyy HH:mm")
        return cell
    }
 
    
    
}
