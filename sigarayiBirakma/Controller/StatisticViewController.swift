//
//  StatisticViewController.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 15.04.2021.
//

import UIKit

func stringtoDateFull(dateString:String)   -> Date{
    let df=DateFormatter()
    df.dateFormat="dd/MM/yyyy HH:mm"
    guard let date=df.date(from: dateString) else { return Date() }
    return date
}

func stringtoDateYear(dateString:String)   -> Date{
    let df=DateFormatter()
    df.dateFormat="yyyy"
    guard let date=df.date(from: dateString) else { return Date() }
    return date
}

class StatisticViewController: UIViewController {

    @IBOutlet weak var btnRestart: UIButton!
    @IBOutlet weak var lblMotive: UILabel!
    
    @IBOutlet weak var lblNumberofCigaratte: UILabel!
    @IBOutlet weak var lblMoneySpend: UILabel!
    @IBOutlet weak var lblLostLife: UILabel!
    
    @IBOutlet weak var lblWeekMoney: UILabel!
    @IBOutlet weak var lblWeekTime: UILabel!
    @IBOutlet weak var lblMonthMoney: UILabel!
    @IBOutlet weak var lblMonthTime: UILabel!
    @IBOutlet weak var lblYearMoney: UILabel!
    @IBOutlet weak var lblYearTime: UILabel!
    @IBOutlet weak var lblFiveYearMoney: UILabel!
    @IBOutlet weak var lblFiveYearTime: UILabel!
    @IBOutlet weak var lblTenYearMoney: UILabel!
    @IBOutlet weak var lblTenYearTime: UILabel!
    @IBOutlet weak var lblTwentyYearMoney: UILabel!
    @IBOutlet weak var lblTwentyYearTime: UILabel!
    
    @IBOutlet weak var lblPercent: UILabel!
    @IBOutlet weak var lblDayInfo: UILabel!
    
    @IBOutlet weak var lblTimernoSmoking: UILabel!
    @IBOutlet weak var lblTimerMoney: UILabel!
    @IBOutlet weak var lblTimerLife: UILabel!
    @IBOutlet weak var lblTimernoSmokingCount: UILabel!
    
    
    weak var shapeLayerLeft: CAShapeLayer?
    weak var shapeLayerRight: CAShapeLayer?
    weak var shapeLayerUp: CAShapeLayer?
    weak var shapeLayerDown: CAShapeLayer?
    
    var smokingTotalDay=DateComponents()
    var smoke=Smoke()
    var timer:Timer?
    var dateNow=Date()
    var dateBegin=Date()
    var diffDate=TimeInterval()
    
    
    var dailySmokingCost = 0.0
    var dailyLostTime = 0
    
    var index=0

    var year = 0
    var month = 0
    var week = 0
    var day = 0
    var hour = 0
    var minute = 0
    var second = 0
   
    @IBOutlet weak var progressBar: CircularProgressBar!
    
    
    @IBOutlet weak var statisticView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressBar.trackClr = UIColor.cyan
        progressBar.progressClr = UIColor.purple
        
        dateBegin=stringtoDateFull(dateString: userDefault.DecisionYear!)
        diffDate = dateNow.timeIntervalSince(dateBegin)
        
        smoke.endYear=stringtoDateFull(dateString: userDefault.DecisionYear!)
        smoke.staryear=stringtoDateYear(dateString: userDefault.SmokingYear!)
        smoke.dailysmoking=Int(userDefault.DailySmoking!)!
        smoke.numberSmoke=Double(userDefault.NumberSmoke!)
        smoke.smokePrice=Double(userDefault.SmokePackPrice!)!
        
        culculateTotalDamage()
        culculateLifeandMoney()
        mainCulculate(diff: diffDate)
        
        
        
    }
    
    private func mainCulculate(diff:TimeInterval){
        
        (year, month, week, day, hour, minute, second) = secondstoFormatDate(seconds: Int(diff))
        lblTimernoSmoking.text = dateString(year: year, month: month, week: month, day: day, hour: hour, minute: minute, second: second)
        
        let timerSmokingEarned = (diff * dailySmokingCost) / 86400
        lblTimerMoney.text = String(format: "%.2f", timerSmokingEarned) + "₺"
        
        
        let timerLifeEarned = (diff * Double(dailyLostTime)) / 86400
        (year, month, week, day, hour, minute, second) = secondstoFormatDate(seconds: Int(timerLifeEarned))
        lblTimerLife.text = dateString(year: year, month: month, week: week, day: day, hour: hour, minute: minute, second: second)
        
      let timerNoSmoking=(diff * Double(smoke.dailysmoking!)) / 86400
       lblTimernoSmokingCount.text = String(format: "%.1f", timerNoSmoking)
     
     
        
         
    }
    
    private func culculateTotalDamage(){
     let calendar = Calendar.current
        smokingTotalDay = calendar.dateComponents([.day], from: smoke.staryear!, to: smoke.endYear!)

        
        let oneCigaratteMoney=smoke.smokePrice!/smoke.numberSmoke!
      
        let totalSmoking = smoke.dailysmoking!*smokingTotalDay.day!
        let totalMoneySpend = oneCigaratteMoney * Double(totalSmoking)
        let totalLostLife = totalSmoking*11*60
        (year, month, week, day, hour, minute, second) = secondstoFormatDate(seconds: totalLostLife)
        
        lblNumberofCigaratte.text="\(totalSmoking)"
        lblMoneySpend.text="\(totalMoneySpend) ₺"
        lblLostLife.text=dateString(year: year, month: month, week: week, day: day, hour: hour, minute: minute, second: second)
        
    }
    
    @IBAction func showLogin(_ sender: Any) {
        performSegue(withIdentifier: "showLogin", sender: nil)
    }
    
   
    
    
    private func culculateLifeandMoney(){
       
        dailySmokingCost = Double(smoke.dailysmoking!) * (smoke.smokePrice!/smoke.numberSmoke!) // günlük sigara zararı para
        dailyLostTime  = smoke.dailysmoking! * 11 * 60 // günlük kaybedilen hayat saniye cinsinden
        
        
        let weekMoney = 7 * dailySmokingCost
        let weekTime = 7 * dailyLostTime
        
        let monthMoney = 30 * dailySmokingCost
        let monthTime = 30 * dailyLostTime
        
        let yearMoney = 365 * dailySmokingCost
        let yearTime = 365 * dailyLostTime
        
        let fiveYearMoney = 1825 * dailySmokingCost
        let fiveYearTime = 1825 * dailyLostTime
        
        let tenYearMoney = 2555 * dailySmokingCost
        let tenYearTime = 2555 * dailyLostTime
        
        let twentyYearMoney = 73000 * dailySmokingCost
        let twentyYearTime = 73000 * dailyLostTime
        
        (year, month, week, day, hour, minute, second) = secondstoFormatDate(seconds: weekTime)
        lblWeekMoney.text="\(weekMoney) ₺"
        lblWeekTime.text=dateString(year: year, month: month, week: week, day: day, hour: hour, minute: minute, second: second)
        
        (year, month, week, day, hour, minute, second) = secondstoFormatDate(seconds: monthTime)
        lblMonthMoney.text="\(monthMoney) ₺"
        lblMonthTime.text=dateString(year: year, month: month, week: week, day: day, hour: hour, minute: minute, second: second)
        
        (year, month, week, day, hour, minute, second) = secondstoFormatDate(seconds: yearTime)
        lblYearMoney.text="\(yearMoney) ₺"
        lblYearTime.text=dateString(year: year, month: month, week: week, day: day, hour: hour, minute: minute, second: second)
        
        (year, month, week, day, hour, minute, second) = secondstoFormatDate(seconds: fiveYearTime)
        lblFiveYearMoney.text="\(fiveYearMoney) ₺"
        lblFiveYearTime.text=dateString(year: year, month: month, week: week, day: day, hour: hour, minute: minute, second: second)
        
        (year, month, week, day, hour, minute, second) = secondstoFormatDate(seconds: tenYearTime)
        lblTenYearMoney.text="\(tenYearMoney) ₺"
        lblTenYearTime.text=dateString(year: year, month: month, week: week, day: day, hour: hour, minute: minute, second: second)
        
        (year, month, week, day, hour, minute, second) = secondstoFormatDate(seconds: twentyYearTime)
        lblTwentyYearMoney.text="\(twentyYearMoney) ₺"
        lblTwentyYearTime.text=dateString(year: year, month: month, week: week, day: day, hour: hour, minute: minute, second: second)
        
        
        
    }
    
    func dateString(year:Int, month:Int, week:Int, day:Int, hour:Int, minute:Int, second:Int) -> String{
        var yearString=""
        var monthString=""
        var weekString=""
        var dayString=""
        var hourString=""
        var minuteString=""
        var secondString=""
        
        var date=""
        
        if year == 0{
            yearString = ""
        }else{
            yearString = "\(year)Y "
        }
        if month == 0{
            monthString = ""
        }else{
            monthString = "\(month)A "
        }
        if week == 0{
            weekString = ""
        }else{
            weekString = "\(week)H "
        }
        if day == 0{
            dayString = ""
        }else{
            dayString = "\(day)G "
        }
        if hour == 0{
            hourString = ""
        }else{
            hourString = "\(hour)S "
        }
        if minute == 0{
            minuteString = ""
        }else{
            minuteString = "\(minute)D "
        }
        if second == 0{
            secondString = ""
        }else{
            secondString = "\(second)Sn"
        }
        
        date = "\(yearString)\(monthString)\(weekString)\(dayString)\(hourString)\(minuteString)\(secondString)"
        
        return date
        
    }
    
    func secondstoFormatDate (seconds : Int) -> (Int, Int, Int, Int, Int, Int, Int) {
       
        let years = seconds / 31557600
        let months = (seconds % 31557600) / 2629800
        let weeks = (seconds % 2629800) / 604800
        let days = (seconds % 604800) / 86400
        let hours = (seconds % 86400) / 3600
        let minutes = (seconds % 3600) / 60
        let seconds = (seconds % 3600) % 60
       
        return (years,months,weeks,days ,hours,minutes,seconds)
    }
    
    private func ConvertSectoTime(index: Int,label:UILabel) {
        let interval = index

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.day,.hour,.minute,.second]
        formatter.unitsStyle = .abbreviated

        let formattedString = formatter.string(from: TimeInterval(interval))!
        label.text=formattedString
        
    }

 
    @IBAction func btnAction(_ sender: UIButton) {
        let ac: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let timeInterval=DateComponents(day:2)
        print(timeInterval)
        
        let twoDay: UIAlertAction = UIAlertAction(title: "2 Gün", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 172800
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="2 Gün"
        }
        let threDay: UIAlertAction = UIAlertAction(title: "3 Gün", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 259200
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="3 Gün"
        }
        let fourDay: UIAlertAction = UIAlertAction(title: "4 Gün", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 345600
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="4 Gün"

        }
        let fiveDay: UIAlertAction = UIAlertAction(title: "5 Gün", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 432000
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="5 Gün"
        }
        let sixDay: UIAlertAction = UIAlertAction(title: "6 Gün", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 518400
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="6 Gün"
        }
        let oneWeek: UIAlertAction = UIAlertAction(title: "1 Hafta", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 604800.02
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="1 Hafta"
        }
        let tenDay: UIAlertAction = UIAlertAction(title: "10 Gün", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 864000
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="10 Gün"
        }
        let twoWeek: UIAlertAction = UIAlertAction(title: "2 Hafta", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 1209600.03
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="2 Hafta"
        }
        let threeWeek: UIAlertAction = UIAlertAction(title: "3 Hafta", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 1814400.05
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="3 Hafta"
        }
        let oneMonth: UIAlertAction = UIAlertAction(title: "1 Ay", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 2629800
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="1 Ay"
        }
        let threeMonth: UIAlertAction = UIAlertAction(title: "3 Ay", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 7889400
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="3 Ay"
        }
        let sixMonth: UIAlertAction = UIAlertAction(title: "6 Ay", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 15778800
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="6 Ay"
        }
        let oneYear: UIAlertAction = UIAlertAction(title: "1 Yıl", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 31557600
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="1 Yıl"
        }
        let fiveYear: UIAlertAction = UIAlertAction(title: "6 Yıl", style: .default) { action -> Void in
            let resultPercent = (100*self.diffDate) / 157788000
            if (resultPercent>100){
                self.lblPercent.text="100%"
            }else{
                self.lblPercent.text=String(format: "%.1f", resultPercent) + "%"
            }
            self.progressBar.setProgressWithAnimation(duration: 1.0, value: Float(resultPercent) / 100)
            self.lblDayInfo.text="6 Yıl"
        }
       
        let cancelAction: UIAlertAction = UIAlertAction(title: "Çıkış", style: .cancel) { action -> Void in }

        // add actions
        ac.addAction(twoDay)
        ac.addAction(threDay)
        ac.addAction(fourDay)
        ac.addAction(fiveDay)
        ac.addAction(sixDay)
        ac.addAction(oneWeek)
        ac.addAction(tenDay)
        ac.addAction(twoWeek)
        ac.addAction(threeWeek)
        ac.addAction(oneMonth)
        ac.addAction(threeMonth)
        ac.addAction(sixMonth)
        ac.addAction(oneYear)
        ac.addAction(fiveYear)
        ac.addAction(cancelAction)
        
        if let popover = ac.popoverPresentationController {
            popover.sourceView = sender
            popover.sourceRect = sender.bounds
        }
        present(ac, animated: true, completion: nil)
    }

}

extension Date {

    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }

}

