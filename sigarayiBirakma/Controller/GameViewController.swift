//
//  GameViewController.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 15.04.2021.
//

import UIKit

class GameViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    var model=CardModel()
    var cardArray=[Card]()
    
    var firsSelectedCard:IndexPath?
    
    var gameSound=SoundManager()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("başlatıldı")
      
        
        cardArray=model.getCard()
        collectionView.delegate=self
        collectionView.dataSource=self
        // Do any additional setup after loading the view.
    }
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCell", for: indexPath) as! CardCollectionViewCell
        
        let card=cardArray[indexPath.row]
        
        cell.setCard(card: card)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! CardCollectionViewCell
        
        let card=cardArray[indexPath.row]
        
        if card.isFlipping==false && card.isMatched==false{
            cell.flip()
            //her kartın çevrilmesi sırasında oluşan ses
            gameSound.playSound(efect: .dokunma)
            card.isFlipping=true
            
            if firsSelectedCard==nil{
                firsSelectedCard=indexPath
            }
            else{
                checkForMatches(secondSelectedCard: indexPath)
            }
            
        }else{
            cell.flipBack()
            card.isFlipping=false
        }
            
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width - 50
        let height = collectionView.frame.height - 50
        
        return CGSize(width: width/4, height: height/4)
    }
    
    
    func checkForMatches(secondSelectedCard:IndexPath){
        let cardOneCell=collectionView.cellForItem(at: firsSelectedCard!) as? CardCollectionViewCell
        
        let cardTwoCell=collectionView.cellForItem(at: secondSelectedCard) as? CardCollectionViewCell
        
        let cardOne=cardArray[firsSelectedCard!.row]
        let cardTwo=cardArray[secondSelectedCard.row]
        
        //Kartlar eşleşmiş demektir.
        if firsSelectedCard!.row != secondSelectedCard.row && cardOne.imageName==cardTwo.imageName{
            gameSound.playSound(efect: .eslesme)
            cardOne.isMatched=true
            cardTwo.isMatched=true
            cardOneCell?.remove()
            cardTwoCell?.remove()
            
            gameEnd()
            
        }else{ //kartlar eşleşmedi
            gameSound.playSound(efect: .eslesmedi)
            cardOne.isFlipping=false
            cardTwo.isFlipping=false
            
            cardOneCell?.flipBack()
            cardTwoCell?.flipBack()
        }
        
        if cardOneCell==nil{
            collectionView.reloadData()
        }
        
        firsSelectedCard=nil
        
    }
    
    func gameEnd(){
        var isWon=true
        
        for card in cardArray{
            if card.isMatched==false{
                isWon=false
                break
            }
        }
        
        var title=""
        var message=""
        
        if isWon==true{
            title="Teprikler"
            message="Oyun başarılı bir şekilde gerçekleştirilmiştir."
            showAlert(title: title, message: message)
        }
       
    }
    func showAlert(title:String,message:String){
        let alert=UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction=UIAlertAction(title: "Tamam", style: .default, handler: { (action) in
            self.cardArray=self.model.getCard()
            self.collectionView.reloadData()
        })
        alert.addAction(alertAction)
        present(alert, animated: true, completion: nil)
    }
  
}




