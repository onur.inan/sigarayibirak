//
//  DailyDetailViewController.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 6.05.2021.
//

import UIKit

class DailyDetailViewController: UIViewController {
    
  
    var dailyData:Daily?
    
    var toolBar = UIToolbar()
    var datePicker  = UIDatePicker()
    var headerTitle=""
    
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var btnDate: MyRoundButton!
    @IBOutlet weak var txtComment: UITextView!
    
    @IBOutlet weak var slider: UISlider!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if headerTitle != ""{
            self.title=headerTitle
            txtComment.text = dailyData?.comment
            btnDate.setTitle(dailyData?.date, for: .normal)
            slider.value=Float (dailyData?.feelingPoint ?? 3)
            lblIndex.text = dailyData?.smokeCount
            
        }else{
            getDate()
        }
   
    }
    
    func getDate(){
        let df=DateFormatter()
        df.dateFormat="dd/MM/yyyy HH:mm"
        let date=Date()
        btnDate.setTitle(df.string(from: date), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
    }
    
    @IBAction func changeSlider(_ sender: UISlider) {
        slider.value=roundf(slider.value)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        if headerTitle==""{
            let daily=Daily(id: "",
                              date: (btnDate.titleLabel?.text!)!,
                              smokeCount: lblIndex.text!,
                              feelingPoint: Int(slider.value),
                              comment: txtComment.text)
              let isSave=DBManager.getInstance().saveDaily(daily: daily)
              print("isSave--> \(isSave)")
            self.navigationController?.popViewController(animated: true)
        }
        else{
            let daily = Daily(id: dailyData!.id,
                              date: (btnDate.titleLabel?.text!)!,
                              smokeCount: lblIndex.text!,
                              feelingPoint: Int(slider.value),
                              comment: txtComment.text)
            let isUpdate=DBManager.getInstance().updateDaily(daily: daily)
            print("isUpdate-->  \(isUpdate)")
            self.navigationController?.popViewController(animated: true)

        }
       
    }
    
    @IBAction func btnPlus(_ sender: Any) {
        var  index = Int(lblIndex.text!)!
        index+=1
        lblIndex.text="\(index)"
     
    }
    @IBAction func btnDown(_ sender: Any) {
        var  index = Int(lblIndex.text!)!
        index-=1
        lblIndex.text="\(index)"
    }
    
    
    @IBAction func btnShowDate(_ sender: Any) {
        datePicker = UIDatePicker.init()
        datePicker.backgroundColor = UIColor.white
                
        datePicker.autoresizingMask = .flexibleWidth
        datePicker.datePickerMode = .dateAndTime
                
        datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        datePicker.frame = CGRect(x: 0.0, y: UIScreen.main.bounds.size.height - 75, width: UIScreen.main.bounds.size.width, height: 100)
        self.view.addSubview(datePicker)
                
        toolBar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 100, width: UIScreen.main.bounds.size.width, height: 25))
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Çıkış", style: .done, target: self, action: #selector(self.onDoneButtonClick))]
        toolBar.sizeToFit()
        self.view.addSubview(toolBar)
    }
    @objc func dateChanged(_ sender: UIDatePicker?) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat="MM-dd-yyyy"
            
        if let date = sender?.date {
            btnDate.setTitle(dateFormatter.string(from: date), for: .normal)
 
        }
    }
 

    @objc func onDoneButtonClick() {
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
    }
    
}
