//
//  DailyViewController.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 23.04.2021.
//

import UIKit

class DailyViewController: UIViewController {
var dailys=[Daily]()
    
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        tblView.delegate=self
        tblView.dataSource=self
      
    }
    override func viewWillAppear(_ animated: Bool) {
        dailys=DBManager.getInstance().getAllDaily()
        tblView.reloadData()
    }

    

}
extension DailyViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dailys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tblView.dequeueReusableCell(withIdentifier: "DailyCell", for: indexPath) as! DailyTableViewCell
        cell.lblComment.text=dailys[indexPath.row].comment
        cell.lblDate.text=dailys[indexPath.row].date
        cell.lblsmokeCount.text=dailys[indexPath.row].smokeCount
        cell.lblfeelingPoint.text="\(dailys[indexPath.row].feelingPoint)"
        cell.btnEdit.addTarget(self, action: #selector(onClickAlertDialog), for: .touchUpInside)
        cell.btnEdit.tag=indexPath.row
        
        return cell
    }
    
    @objc func onClickAlertDialog(sender:UIButton){
        let ac: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Düzenle", style: .default) { action -> Void in
            
            let vc = self.storyboard?.instantiateViewController(identifier: "DailyDetailViewController") as! DailyDetailViewController
            vc.dailyData = self.dailys[sender.tag]
            vc.headerTitle="Girdi Düzenle"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }

        let secondAction: UIAlertAction = UIAlertAction(title: "Sil", style: .default) { action -> Void in
            let isDelete=DBManager.getInstance().deleteDaily(daily: self.dailys[sender.tag])
            self.dailys.remove(at: sender.tag)
            self.tblView.reloadData()
            print("isDelete--> \(isDelete)")
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: "Çıkış", style: .cancel) { action -> Void in }

        // add actions
        ac.addAction(firstAction)
        ac.addAction(secondAction)
        ac.addAction(cancelAction)
        present(ac, animated: true, completion: nil)
    }
    
}

   
    

