//
//  SmokeReasonsViewController.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 10.05.2021.
//

import UIKit

class SmokeReasonsDetailsViewController: UIViewController {

    @IBOutlet weak var txtReason: UITextView!
    
    var reasonData:Reason?
    var headerTitle=""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if headerTitle != ""{
            self.title=headerTitle
            txtReason.text=reasonData?.smokeReason
            
        }

        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSave(_ sender: Any) {
        if headerTitle != "" {
            let reason=Reason(id: reasonData!.id, smokeReason: txtReason.text)
            let isUpdate=DBManager.getInstance().updateReason(reason: reason)
            print("isUpdate ---- \(isUpdate)")
            self.navigationController?.popViewController(animated: true)
        }else{
            let reason=Reason(id: "", smokeReason: txtReason.text ?? "")
            let isSave=DBManager.getInstance().saveReason(reason: reason)
            print("SmokeReasons Save -->\(isSave)")
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
