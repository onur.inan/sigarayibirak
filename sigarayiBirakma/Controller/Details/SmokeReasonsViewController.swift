//
//  SmokeReasonsViewController.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 10.05.2021.
//

import UIKit

class SmokeReasonsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var reasons=[Reason]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate=self
        tableView.dataSource=self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reasons=DBManager.getInstance().getAllReason()
        tableView.reloadData()
    }
   

}
extension SmokeReasonsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "reasonCell", for: indexPath) as! SmokeReasonTableViewCell
        cell.btnIndex.setTitle(String(indexPath.row+1), for: .normal)
        cell.lblText.text=reasons[indexPath.row].smokeReason
        cell.btnEdit.addTarget(self, action: #selector(onClickAlertDialog), for: .touchUpInside)
        cell.btnEdit.tag=indexPath.row
        return cell
    }
    
    @objc func onClickAlertDialog(sender:UIButton){
        let ac: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Düzenle", style: .default) { action -> Void in
            
            let vc = self.storyboard?.instantiateViewController(identifier: "SmokeReasonsDetailsViewController") as! SmokeReasonsDetailsViewController
            vc.reasonData=self.reasons[sender.tag]
            vc.headerTitle="Güncelleme"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }

        let secondAction: UIAlertAction = UIAlertAction(title: "Sil", style: .default) { action -> Void in
            let isDelete=DBManager.getInstance().deleteReason(reason: self.reasons[sender.tag])
            self.reasons.remove(at: sender.tag)
            self.tableView.reloadData()
            print("isDelete--> \(isDelete)")
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: "Çıkış", style: .cancel) { action -> Void in }

        // add actions
        ac.addAction(firstAction)
        ac.addAction(secondAction)
        ac.addAction(cancelAction)
        present(ac, animated: true, completion: nil)
    }
    
    
}
