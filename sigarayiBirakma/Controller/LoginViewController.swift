//
//  LoginViewController.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 26.04.2021.
//

import UIKit

class  LoginViewController: UIViewController, UITextFieldDelegate{
    var toolBar = UIToolbar()
    var datePicker  = UIDatePicker()

    
    @IBOutlet weak var txtFileDailySmoking: UITextField!
    @IBOutlet weak var txtFileNumberSmoke: UITextField!
    @IBOutlet weak var txtFileSmokingYear: UITextField!
    @IBOutlet weak var txtFileSmokePackPrice: UITextField!
    
    
    @IBOutlet weak var btnDate: UIButton!
      @IBOutlet weak var txtFieldDate: UITextField!
    
    var datepicker:UIDatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDate()
        
    }
    
    func getDate(){
        let df=DateFormatter()
        df.dateFormat="dd/MM/yyyy HH:mm"
        let date=Date()
        btnDate.setTitle(df.string(from: date), for: .normal)
    }
    @IBAction func btnDate(_ sender: Any) {
            datePicker = UIDatePicker.init()
            datePicker.backgroundColor = UIColor.white
                    
            datePicker.autoresizingMask = .flexibleWidth
            datePicker.datePickerMode = .dateAndTime
                    
            datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
            datePicker.frame = CGRect(x: 0.0, y: UIScreen.main.bounds.size.height - 75, width: UIScreen.main.bounds.size.width, height: 100)
            self.view.addSubview(datePicker)
                    
            toolBar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 100, width: UIScreen.main.bounds.size.width, height: 25))
            toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Çıkış", style: .done, target: self, action: #selector(self.onDoneButtonClick))]
            toolBar.sizeToFit()
            self.view.addSubview(toolBar)
       
       
    }
    
    @objc func dateChanged(_ sender: UIDatePicker?) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat="dd/MM/yyyy HH:mm"
            
        if let date = sender?.date {
            btnDate.setTitle(dateFormatter.string(from: date), for: .normal)
        }
    }

    @objc func onDoneButtonClick() {
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
    }
    
    
    @IBAction func btnIndex(_ sender: UIButton) {
        var index = 0
        var pointIndex = 0.0
        
        if sender.tag == 0 {
            index=Int(txtFileDailySmoking.text!) ?? 0
            index -= 1
            txtFileDailySmoking.text = "\(index)"
            
        }
        if sender.tag == 1 {
            index=Int(txtFileDailySmoking.text!) ?? 0
            index += 1
            txtFileDailySmoking.text = "\(index)"
        }
        if sender.tag == 2 {
            index=Int(txtFileNumberSmoke.text!) ?? 0
            index -= 1
            txtFileNumberSmoke.text = "\(index)"
        }
        if sender.tag == 3  {
            index=Int(txtFileNumberSmoke.text!) ?? 0
            index += 1
            txtFileNumberSmoke.text = "\(index)"
        }
        if sender.tag == 4 {
            index=Int(txtFileSmokingYear.text!) ?? 0
            index -= 1
            txtFileSmokingYear.text = "\(index)"
        }
        if sender.tag == 5  {
            index=Int(txtFileSmokingYear.text!) ?? 0
            index += 1
            txtFileSmokingYear.text = "\(index)"
        }
        if sender.tag == 6 {
            pointIndex=Double(txtFileSmokePackPrice.text!) ?? 0.0
            pointIndex -= 0.5
            txtFileSmokePackPrice.text = "\(pointIndex)"
        }
        if sender.tag == 7  {
            pointIndex=Double(txtFileSmokePackPrice.text!) ?? 0.0
            pointIndex += 0.5
            txtFileSmokePackPrice.text = "\(pointIndex)"
        }
    }
    
    
    @IBAction func btnOk(_ sender: Any){
        var title=""
        var message=""
        let smokingYearCount=txtFileSmokingYear.text?.count
        let smokingYear=Int(txtFileSmokingYear.text ?? "0") ?? 0
        
        let currentYear = Calendar.current.component(.year, from: Date())
        
        if(isEmpty()==true || smokingYearCount != 4 || smokingYear<1900 || currentYear<smokingYear){
            title="Hata"
            message="Bilgilerinizi lütfen kontrol ediniz!"
            showAlert(title: title, message: message)
            
        }else{
            userDefault.DailySmoking=txtFileDailySmoking.text
            userDefault.NumberSmoke=txtFileNumberSmoke.text
            userDefault.SmokingYear=txtFileSmokingYear.text
            userDefault.SmokePackPrice=txtFileSmokePackPrice.text
            userDefault.DecisionYear=btnDate.titleLabel?.text
            
            dismiss(animated: true, completion: nil)
        }
    }
    
    func isEmpty()->Bool{
        if (txtFileDailySmoking.text == "" || txtFileNumberSmoke.text == "" || txtFileSmokingYear.text == "" || txtFileSmokePackPrice.text == ""
        )
         {
          return true
        }else{
            return false
        }
    }
    
    func showAlert(title:String,message:String){
        let alert=UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction=UIAlertAction(title: "Tamam", style: .default, handler: nil)
        alert.addAction(alertAction)
        present(alert, animated: true, completion: nil)
    }
    
     @objc func dateShow(datePicker:UIDatePicker){
        let df=DateFormatter()
        df.dateFormat="dd/MM/yyyy HH:mm"
        let getDate=df.string(from: datePicker.date)
        print(getDate)
        
        btnDate.setTitle(getDate, for: .normal)
    }
       
  
  
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}
