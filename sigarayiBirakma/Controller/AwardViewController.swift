//
//  AwardViewController.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 18.05.2021.
//

import UIKit

class AwardViewController: UIViewController {
    var awards=[Award]()
    
    @IBOutlet weak var lblMoney: UILabel!
    @IBOutlet weak var tableView: UITableView!
    let alertService = AlertService()
    
    var smoke = Smoke()
    var dailySmokingCost = 0.0
    var timerSmokingEarned = 0.0
  
    override func viewDidLoad() {
        super.viewDidLoad()
        lblMoney.text="200"
        tableView.delegate=self
        tableView.dataSource=self
    }
  
    override func viewWillAppear(_ animated: Bool) {
        awards=DBManager.getInstance().getAllAward()
        tableView.reloadData()
        
        if userDefault.DecisionYear != nil {
            smoke.endYear=stringtoDateFull(dateString: userDefault.DecisionYear!)
            smoke.staryear=stringtoDateYear(dateString: userDefault.SmokingYear!)
            smoke.dailysmoking=Int(userDefault.DailySmoking!)!
            smoke.numberSmoke=Double(userDefault.NumberSmoke!)
            smoke.smokePrice=Double(userDefault.SmokePackPrice!)!
            dailySmokingCost = Double(smoke.dailysmoking!) * (smoke.smokePrice!/smoke.numberSmoke!) // günlük sigara zararı para
            
            let dateNow=Date()
            let dateBegin = stringtoDateFull(dateString: userDefault.DecisionYear!)
            let diffDate = dateNow.timeIntervalSince(dateBegin)
            mainCulculate(diff: diffDate)
        }
    }
    
    private func mainCulculate(diff:TimeInterval){
        timerSmokingEarned = ((diff * dailySmokingCost) / 86400) - userDefault.spentMoney
        if timerSmokingEarned < 0 {
            timerSmokingEarned = 0
        }
        lblMoney.text = String(format: "%.2f", timerSmokingEarned)
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        
        let alertVC=alertService.alert {
            self.awards=DBManager.getInstance().getAllAward()
            self.tableView.reloadData()
        }
        present(alertVC, animated: true)
       
    }
 
    func showAlert(title:String,message:String){
        let alert=UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction=UIAlertAction(title: "Tamam", style: .default, handler: nil)
        alert.addAction(alertAction)
        present(alert, animated: true, completion: nil)
    }
   
  

}
extension AwardViewController: UITableViewDelegate  ,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return awards.count
    }
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "AwardCell", for: indexPath) as! AvardTableViewCell
    

        let totalMoney=Double(lblMoney.text!) ?? 0
        let ownMoney=Double(awards[indexPath.row].money) ?? 0
        let percent = (100 * totalMoney) / ownMoney
        
        if(percent>100){
            cell.lblPercent.text="\(100)%"
            cell.progressProportion.progress=Float(100)
        }else{
            cell.lblPercent.text = String(format: "%.2f", percent) + "%"
            cell.progressProportion.progress=Float(percent/1000)
        }
        cell.lblProductName.text=awards[indexPath.row].name

        cell.lblProductMoney.text="\(awards[indexPath.row].money) ₺"

        cell.btnDetails.tag=indexPath.row
        cell.btnDetails.addTarget(nil, action: #selector(onClickAlertDialog), for: .touchUpInside)
        cell.btnBuy.tag=indexPath.row
        cell.btnBuy.addTarget(nil, action: #selector(onClickBuy), for: .touchUpInside)
        
        
        return cell
    }

    
   
    @objc func onClickBuy(sender:UIButton){
        let totalMoney=Double(lblMoney.text!) ?? 0
        let ownMoney=Double(awards[sender.tag].money) ?? 0
        let money=totalMoney-ownMoney
        
        if money.sign == .minus{
            showAlert(title: "Yetersiz Bakiye", message: "Mevcut bakiyeniz yetersiz lütfen daha sonra tekrar deniyinin!")
        }else{
            lblMoney.text="\(money)"
            
            let isDelete=DBManager.getInstance().deleteAward(award: self.awards[sender.tag])
            self.awards.remove(at: sender.tag)
            self.tableView.reloadData()
            print("isDelete--> \(isDelete)")
            tableView.reloadData()
            userDefault.spentMoney = userDefault.spentMoney + ownMoney
        }
        
        
    }
        
    
    
    @objc func onClickAlertDialog(sender:UIButton){
        let ac: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)


        let firstAction: UIAlertAction = UIAlertAction(title: "Sil", style: .default) { action -> Void in
            let isDelete=DBManager.getInstance().deleteAward(award: self.awards[sender.tag])
            self.awards.remove(at: sender.tag)
            self.tableView.reloadData()
            print("isDelete--> \(isDelete)")
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: "Çıkış", style: .cancel) { action -> Void in }

        // add actions
        ac.addAction(firstAction)
        ac.addAction(cancelAction)
        present(ac, animated: true, completion: nil)
    }
  
    
    
}
 
