//
//  MyTabBarVC.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 19.04.2021.
//

import UIKit

class MyTabBarVC: UITabBarController {

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if userDefault.DecisionYear==nil{
            performSegue(withIdentifier: "showLogin", sender: nil)
        }
    }
    
}

