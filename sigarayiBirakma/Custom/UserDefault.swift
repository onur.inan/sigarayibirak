//
//  UserDefault.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 30.04.2021.
//
import Foundation
public let userDefault = UserDefault.sharedInstance

public class UserDefault {

    public static let sharedInstance = UserDefault()
    let userDefaults = UserDefaults.standard
  
    

    
    public var DailySmoking: String? {
        get {
            return valueFromDatabase("dailysmoking")
        }
        
        set(value) {
            storeValue(value, forKey: "dailysmoking")
        }
    }
    public var NumberSmoke: String? {
        get {
            return valueFromDatabase("numbersmoke")
        }
        
        set(value) {
            storeValue(value, forKey: "numbersmoke")
        }
    }
    public var SmokingYear: String? {
        get {
            return valueFromDatabase("smokingyear")
        }
        
        set(value) {
            storeValue(value, forKey: "smokingyear")
        }
    }
    public var SmokePackPrice: String? {
        get {
            return valueFromDatabase("smokepackprice")
        }
        
        set(value) {
            storeValue(value, forKey: "smokepackprice")
        }
    }
    public var DecisionYear: String? {
        get {
            return valueFromDatabase("decisionyear")
        }
        
        set(value) {
            storeValue(value, forKey: "decisionyear")
        }
    }
    
    public var spentMoney: Double! {
        get {
            return valueFromDatabase("spentMoney") ?? 0
        }
        
        set(value) {
            storeValue(value, forKey: "spentMoney")
        }
    }
   
  
    func storeValue(_ value : Double?, forKey : String) {
        userDefaults.set(value, forKey: forKey)
        userDefaults.synchronize()
    }
    
    func storeValue(_ value : String?, forKey : String) {
        userDefaults.set(value, forKey: forKey)
        userDefaults.synchronize()
    }
    
    func storeValue(_ value : [String], forKey : String) {
        userDefaults.set(value, forKey: forKey)
        userDefaults.synchronize()
    }
    
    func storeValue(_ value : Bool, forKey : String) {
        userDefaults.set(value, forKey: forKey)
        userDefaults.synchronize()
    }
    
    func storeValue(_ value : Int, forKey : String) {
        userDefaults.set(value, forKey: forKey)
        userDefaults.synchronize()
    }
    
    func storeValue(_ value : [Int], forKey : String) {
        userDefaults.set(value, forKey: forKey)
        userDefaults.synchronize()
    }
    
    func storeValue(_ value : Date, forKey : String) {
        userDefaults.set(value, forKey: forKey)
        userDefaults.synchronize()
    }
    
    func valueFromDatabase (_ key: String) -> String? {
        return userDefaults.object(forKey: key) as? String
    }
    
    func valueFromDatabase (_ key: String) -> [String]? {
        return userDefaults.object(forKey: key) as? [String]
    }
    
    func valueFromDatabase (_ key: String) -> Bool? {
        return userDefaults.object(forKey: key) as? Bool
    }
    func valueFromDatabase (_ key: String ) -> Double? {
        return userDefaults.object(forKey: key) as? Double
    }
    
    
    func valueFromDatabase (_ key: String) -> Int? {
        return userDefaults.object(forKey: key) as? Int
    }
    
    func valueFromDatabase (_ key: String) -> [Int]? {
        return userDefaults.object(forKey: key) as? [Int]
    }
    
    func valueFromDatabase (_ key: String) -> Date? {
        return userDefaults.object(forKey: key) as? Date
    }
    
    func removeDataFromDatabase (_ key : String) {
        userDefaults.removeObject(forKey: key)
        userDefaults.synchronize()
    }
}
