//
//  MyButton.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 26.04.2021.
//

import UIKit

class MyButton: UIButton {

    var myView = UIView()
    var toolBarView = UIView()

    override var inputView: UIView {
        get {
            return self.myView
        }

        set {
            self.myView = newValue
            self.becomeFirstResponder()
        }
    }

    override var inputAccessoryView: UIView {
        get {
            return self.toolBarView
        }
        set {
            self.toolBarView = newValue
        }
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }

}

@IBDesignable class MyRoundButton : UIButton{

    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }

    override func prepareForInterfaceBuilder() {
        sharedInit()
    }

    func sharedInit() {
        refreshCorners(value: cornerRadius)
    }

    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }

    @IBInspectable var cornerRadius: CGFloat = 15 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
}


