//
//  AlertService.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 18.05.2021.
//

import UIKit
class AlertService {
    
    
    
    func alert( complation: @escaping() -> Void) -> AlertViewController   {
     
        let storyboard = UIStoryboard(name: "Alert", bundle: .main)
        let alertVC = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
                
        
        alertVC.btnAction=complation
        return alertVC
        
    }

    
 
}
