//
//  Extentions.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 21.05.2021.
//

import Foundation

extension Date{
    func toString(fromFormat:String , toFormat: String="dd/MM/yyyy HH:mm") -> String{
        
        let df=DateFormatter()
        df.dateFormat=fromFormat
        let dateString=df.string(from: self)
        let againDate=df.date(from: dateString)
        df.dateFormat=toFormat
        
        let outDate=df.string(from: againDate!)
        return outDate
        
    }
    
    func addDaytoDate(byTimes:Int) -> Date?{
        let nextDate : Date = Calendar.current.date(byAdding: .day, value: byTimes, to: self)!
        return nextDate
    }
    
    func addMinutetoDate(byTimes:Int) -> Date?{
        let nextDate : Date = Calendar.current.date(byAdding: .minute, value: byTimes, to: self)!
        return nextDate
    }
    
    
}
