//
//  CardModel.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 19.04.2021.
//

import Foundation


class CardModel{
    
    func getCard()-> [Card]{
        // oluşturulan kartları saklamak için dizi bildirilmesi
        var generateCardArray = [Card]()
        
        //rastgele kart çiftlerinin oluşturulması
        for _ in 1...8{
            
            let randomnumber = arc4random_uniform(13)+1 // 0 and 4,294,967,295 arasında rastgele sayılar üretmektedir.
            
            //birinci kartı oluşturma
            let cardOne=Card()
            cardOne.imageName="card\(randomnumber)" //assent içierisinde card ile başalayanaları sonuna random sayı eklenmesi
            generateCardArray.append(cardOne)
            
            //ikinciş kartı oluşturma
            let cardTwo=Card()
            cardTwo.imageName="card\(randomnumber)" //assent içierisinde card stringi ile başalayanaları sonuna random sayı eklenmesi
            generateCardArray.append(cardTwo)
        }
        
        //diziyi karıştırma
        generateCardArray.shuffle()
        
        //geriye diziyi döndürme
        return generateCardArray
        
    }
   
    
}
