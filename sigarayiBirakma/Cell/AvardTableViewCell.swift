//
//  AvardTableViewCell.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 18.05.2021.
//

import UIKit

class AvardTableViewCell: UITableViewCell {

    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductMoney: UILabel!
    @IBOutlet weak var lblAvailableDate: UILabel!
    @IBOutlet weak var lblPercent: UILabel!
    
    @IBOutlet weak var progressProportion: UIProgressView!
    
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var btnBuy: UIButton!
    
    override open var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame =  newFrame
            frame.origin.y += 10
            frame.origin.x += 10
            frame.size.height -= 15
            frame.size.width -= 2 * 10
            super.frame = frame
        }
    }

    override open func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
        layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

  
}
