//
//  SmokeReasonTableViewCell.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 10.05.2021.
//

import UIKit

class SmokeReasonTableViewCell: UITableViewCell {

    @IBOutlet weak var btnIndex: UIButton!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
