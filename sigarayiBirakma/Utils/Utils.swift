//
//  Utils.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 6.05.2021.
//

import Foundation
import UIKit

let share = Util()

class Util
{
    // veri tabanı dosya yolunu bulunulması
    
    func getPath(dbName: String) -> String
    {
        let documanDizin = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] //dizin tanımlama
        let fileUrl=documanDizin.appendingPathComponent(dbName)
        print(fileUrl.path)
        return fileUrl.path
        
    }
    
    //veri tabanı yoksa veri tabanı oluşturma yolu
    public func copyDatabase(){
   
        let dbName = "smokeDB.sqlite"
        let dbPath = getPath(dbName: dbName)
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: dbPath){
            let bundle=Bundle.main.resourceURL
            let file=bundle?.appendingPathComponent(dbName)
            do{
                try fileManager.copyItem(atPath: file!.path, toPath: dbPath)
            }
            catch let err{
                print(err.localizedDescription)
            }
        }
    }
    
}
