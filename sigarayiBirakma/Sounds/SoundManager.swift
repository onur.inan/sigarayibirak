//
//  SoundManager.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 21.04.2021.
//

import Foundation
import AVFoundation

class SoundManager{
    
    var audioPlayer:AVAudioPlayer?
    
    enum SoundEffect {
        case dokunma
        case eslesme
        case eslesmedi
        case yedek2
    }
    
    func playSound(efect:SoundEffect){
        
        var soundFileName=""
        
        switch efect {
        case .dokunma:
            soundFileName="cardflip"
        case .eslesme:
            soundFileName="dingcorrect"
        case .eslesmedi:
            soundFileName="dingwrong"
        case .yedek2:
            soundFileName="suffle"
        }
        
        //projenin açıldıgı esnada seslerin dosya yolunun belirlenmesi
        let bundlePath=Bundle.main.path(forResource: soundFileName, ofType: "wav")
        
        guard (bundlePath != nil) else  {
            print("\(soundFileName) isimli ses dosyası bulunamadı")
            return
        }
        //paketin içindeki ses dosyasını bulunması
        let soundURL=URL(fileURLWithPath: bundlePath!)
        
        do {
            //ses çalar nesnesinin oluşturulması
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
             
            audioPlayer?.play()
        }
        catch  {
            //ses dosyası oluşuturulması sonucunda hata meydana gelirse
            print("ses dosyası oluşturalamadı!!! \(soundFileName)")
            
        }
    
       
        
    }
        
    
}
