//
//  DBManager.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 6.05.2021.
//

import Foundation
import UIKit
import FMDB


var shareInstance = DBManager()
   

class DBManager{

    var database:FMDatabase?=nil
  
    static func getInstance()->DBManager {
        if shareInstance.database == nil{
            shareInstance.database=FMDatabase(path: share.getPath(dbName: "smokeDB.sqlite"))
        }
        return shareInstance
    }
    
    //günlük veri tabanına kaydetme
    func saveDaily(daily: Daily)->Bool{
        shareInstance.database?.open()
        let isSave = shareInstance.database?.executeUpdate("INSERT INTO DailyInfo(daily_date,daily_smokeCount,daily_feelingPoint,daily_comment) VALUES (?,?,?,?)", withArgumentsIn : [daily.date , daily.smokeCount,daily.feelingPoint,daily.comment])
        shareInstance.database?.close()
        return isSave!
    }
    
    //günlükleri listeleme
    func getAllDaily()->[Daily]{
        shareInstance.database?.open()
        var dailys=[Daily]()
        do{
            let resultset : FMResultSet? = try shareInstance.database?.executeQuery("SELECT * FROM DailyInfo", values: nil)
            //(resultset!.int(forColumn: "daily_id")!)
            if resultset != nil {
                while resultset!.next(){
                    let daily = Daily(id: (resultset!.string(forColumn: "daily_id")!),
                                     date: (resultset!.string(forColumn: "daily_date")!),
                                     smokeCount: (resultset!.string(forColumn: "daily_smokeCount")!),
                                     feelingPoint: Int((resultset!.int(forColumn: "daily_feelingPoint"))),
                                     comment: (resultset!.string(forColumn: "daily_comment")!))
                   
                    dailys.append(daily)
                }
            }
            
        }
        
        catch let err{
            print(err.localizedDescription)
        }
        shareInstance.database?.close()
        return dailys
    }
    
    
    //günlük güncelleme
    func updateDaily(daily: Daily) -> Bool{
        shareInstance.database?.open()
        let isUdate = shareInstance.database?.executeUpdate("UPDATE DailyInfo SET daily_date=?, daily_smokeCount=?,daily_feelingPoint=?,daily_comment=? WHERE daily_id=?", withArgumentsIn: [
                                                                daily.date,
                                                                daily.smokeCount,
                                                                daily.feelingPoint,
                                                                daily.comment,
                                                                daily.id])
        
        shareInstance.database?.close()
        return isUdate!
    }
    
    
    //öğrencileri silme
    
    func deleteDaily(daily:Daily)->Bool{
        shareInstance.database?.open()
        let isDelete = shareInstance.database?.executeUpdate("DELETE FROM DailyInfo WHERE daily_id=?", withArgumentsIn: [daily.id])
        shareInstance.database?.close()
        return isDelete!
    }
    
    
    func getAllReason()->[Reason]{
        shareInstance.database?.open()
        var reasons=[Reason]()
        do{
            let resultset : FMResultSet? = try shareInstance.database?.executeQuery("SELECT * FROM SmokeReasons", values: nil)
            //(resultset!.int(forColumn: "daily_id")!)
            if resultset != nil {
                while resultset!.next(){
                    let reason = Reason(id: resultset!.string(forColumn: "id")!,
                                       smokeReason: resultset!.string(forColumn: "smoke_reason")!)
                   
                    reasons.append(reason)
                }
            }
            
        }
        
        catch let err{
            print(err.localizedDescription)
        }
        shareInstance.database?.close()
        return reasons
    }
    
    func saveReason(reason: Reason)->Bool{
        shareInstance.database?.open()
        let isSave = shareInstance.database?.executeUpdate("INSERT INTO SmokeReasons(smoke_reason) VALUES (?)", withArgumentsIn : [reason.smokeReason])
        shareInstance.database?.close()
        return isSave!
    }
    func updateReason(reason: Reason)->Bool{
        shareInstance.database?.open()
        let isUpdate = shareInstance.database?.executeUpdate("UPDATE SmokeReasons SET smoke_reason=? WHERE id=?", withArgumentsIn :  [
                                                            reason.smokeReason,
                                                            reason.id])
        shareInstance.database?.close()
        return isUpdate!
    }
    func deleteReason(reason:Reason)-> Bool{
        shareInstance.database?.open()
        let isDelete=shareInstance.database?.executeUpdate("DELETE FROM SmokeReasons WHERE id=?", withArgumentsIn: [reason.id])
        shareInstance.database?.close()
        return isDelete!
    }
    
    //ödülleri listeleme
    func getAllAward()->[Award]{
        shareInstance.database?.open()
        var awards=[Award]()
        do{
            let resultset : FMResultSet? = try shareInstance.database?.executeQuery("SELECT * FROM AwardInfo", values: nil)
            if resultset != nil {
                while resultset!.next(){
                    let award = Award(id: (resultset!.string(forColumn: "id")!),
                                     name: (resultset!.string(forColumn: "name")!),
                                     money: (resultset!.string(forColumn: "money")!))
                   
                    awards.append(award)
                }
            }
            
        }
        
        catch let err{
            print(err.localizedDescription)
        }
        shareInstance.database?.close()
        return awards
    }
    
    //ödül veri tabanına kaydetme
    func saveAward(award: Award)->Bool{
        shareInstance.database?.open()
        let isSave = shareInstance.database?.executeUpdate("INSERT INTO AwardInfo(name,money) VALUES (?,?)", withArgumentsIn : [award.name,award.money])
        shareInstance.database?.close()
        return isSave!
    }
    
    //ödülleri silme
    func deleteAward(award:Award)-> Bool{
        shareInstance.database?.open()
        let isDelete=shareInstance.database?.executeUpdate("DELETE FROM AwardInfo WHERE id=?", withArgumentsIn: [award.id])
        shareInstance.database?.close()
        return isDelete!
    }
    
    func getAllHeardData()->[Heard]{
        shareInstance.database?.open()
        var heards=[Heard]()
        do{
            let resultset : FMResultSet? = try shareInstance.database?.executeQuery("SELECT * FROM HeardInfo", values: nil)
            //(resultset!.int(forColumn: "daily_id")!)
            if resultset != nil {
                while resultset!.next(){
                    let heard=Heard(id: (resultset!.string(forColumn: "id")!),
                                    info: (resultset!.string(forColumn: "info")!) ,
                                    timeSecond: Int((resultset!.int(forColumn: "timeSecond"))))
                                  
                    heards.append(heard)
                }
            }
            
        }
        
        catch let err{
            print(err.localizedDescription)
        }
        shareInstance.database?.close()
        return heards
    }
}

