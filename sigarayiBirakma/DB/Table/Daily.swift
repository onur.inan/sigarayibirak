//
//  Daily.swift
//  sigarayiBirakma
//
//  Created by Onur İnan on 6.05.2021.
//

import Foundation
struct Daily {
    var id:String
    var date:String
    var smokeCount:String
    var feelingPoint:Int
    var comment:String
}
